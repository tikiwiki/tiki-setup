<?php

declare(strict_types=1);

namespace App\FileSystem;

use Symfony\Component\Process\Process;

class UnixFileSystem extends CustomFileSystem
{
    /**
     * @param string $file
     * @param string $mode
     * @param null   $output
     * @param int    $timeout
     *
     * @return bool
     */
    public function chmodOS($file, $mode, &$output = null, $timeout = 900)
    {
        $process = new Process(['chmod', '-R', $mode, $file]);
        $process->setTimeout($timeout);
        $process->run();
        $output = $process->getErrorOutput();

        return $process->isSuccessful();
    }
}
