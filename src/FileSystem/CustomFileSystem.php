<?php

declare(strict_types=1);

namespace App\FileSystem;

use Symfony\Component\Filesystem\Filesystem;

abstract class CustomFileSystem extends Filesystem
{
    /**
     * @param $file
     * @param $mode
     * @param null $output
     *
     * @return mixed
     */
    abstract public function chmodOS($file, $mode, &$output = null);
}
