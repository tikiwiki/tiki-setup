<?php

declare(strict_types=1);

namespace App\Composer;

use App\Composer\Exception\ComposerDownloadException;
use App\Composer\Exception\ComposerNotFoundException;
use App\Utils;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;

class Composer
{
    use Utils;

    const COMPOSER_SETUP = 'composer-setup.php';

    const COMPOSER_PHAR = 'composer.phar';

    const COMPOSER_HOME = 'composer';

    private $installerURL;

    private $signatureURL;

    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * @var string
     */
    private $tempDir;

    /**
     * ComposerDownloader constructor.
     *
     * @param string $dir The path where composer.phar is located or will be stored
     */
    public function __construct($dir, OutputInterface $output = null)
    {
        $this->tempDir = $dir;
        $this->output = $output ?: new NullOutput();
    }

    public function setInstallerURL(string $url)
    {
        $this->installerURL = $url;
    }

    public function setInstallerSignatureUrl(string $url)
    {
        $this->signatureURL = $url;
    }

    /**
     * Download composer phar.
     *
     * @param int $timeout
     *
     * @throws ComposerDownloadException
     *
     * @return string
     */
    public function download($timeout = 900)
    {
        $expectedSig = \trim(\file_get_contents($this->signatureURL));

        $tempDir = $this->tempDir;
        $setupFile = $tempDir.'/'.static::COMPOSER_SETUP;

        try {
            $fs = new Filesystem();
            $fs->mkdir(\dirname($setupFile));

            if (!\copy($this->installerURL, $setupFile)) {
                throw new \Exception('Failed to copy file to '.$setupFile);
            }
        } catch (\Exception $e) {
            $message = \sprintf('Unable to download composer installer from %s', $this->installerURL);
            $message .= PHP_EOL.$e->getMessage();
            throw new ComposerDownloadException($message);
        }

        $actualSig = \hash_file('SHA384', $setupFile);

        if ($expectedSig !== $actualSig) {
            \unlink($setupFile);
            throw new ComposerDownloadException('Invalid composer installer signature.');
        }

        $env = null;
        if (!\getenv('COMPOSER_HOME')) {
            $env['COMPOSER_HOME'] = $tempDir.'/'.static::COMPOSER_HOME;
        }
        $env['HTTP_ACCEPT_ENCODING'] = '';

        $command = [PHP_BINARY, $setupFile, '--quiet', '--install-dir='.$this->tempDir];
        $process = new Process($command, null, $env);
        $process->setTimeout($timeout);
        $process->run();

        \unlink($setupFile);

        if (!$process->isSuccessful()) {
            throw new ComposerDownloadException('There was a problem when installing Composer.');
        }

        return $tempDir.'/'.static::COMPOSER_PHAR;
    }

    public function installDependencies($cwd, $extraArgs = [], $timeout = 900)
    {
        $composer = $this->getBin();

        if (!$composer) {
            throw new ComposerNotFoundException('Composer can not be found.');
//            // Download composer.phar
//            $this->download();
//            $composer = $this->getBin();
        }

        $parts = \array_merge(
            $composer,
            \array_merge(['install', '--no-interaction', '--prefer-dist', '--no-progress'], $extraArgs)
        );

        $process = new Process($parts, $cwd, null, null, $timeout);
        $process->start();
        $process->wait(function ($type, $buffer): void {
            $this->output->write($buffer);
        });

        return $process->getExitCode();
    }

    /**
     * Sets the Console Output.
     */
    public function setOutput(OutputInterface $output)
    {
        $this->output = $output;
    }

    protected function getBin()
    {
        if (\file_exists($this->tempDir.'/composer.phar')) {
            return [PHP_BINARY, $this->tempDir.'/composer.phar'];
        }

        $shell = $this->verifyCommand('composer');

        return $shell ? [$shell] : null;
    }
}
