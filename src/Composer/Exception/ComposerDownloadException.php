<?php

declare(strict_types=1);

namespace App\Composer\Exception;

class ComposerDownloadException extends ComposerException
{
}
