<?php

declare(strict_types=1);

namespace App\Composer\Exception;

use App\Exception\AppException;

class ComposerException extends AppException
{
}
