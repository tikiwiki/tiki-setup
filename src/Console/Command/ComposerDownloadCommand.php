<?php

declare(strict_types=1);

namespace App\Console\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ComposerDownloadCommand extends BaseCommand
{
    protected static $defaultName = 'composer:download';

    protected function configure(): void
    {
        parent::configure();

        $this
            ->setDescription('Download composer.phar.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $container = $this->getApplication()->getContainer();
        $composer = $container->get('Composer');

        $composer->download();
        $this->io->success('Composer downloaded successfully.');

        return 0;
    }
}
