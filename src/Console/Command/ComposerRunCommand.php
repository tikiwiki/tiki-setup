<?php

declare(strict_types=1);

namespace App\Console\Command;

use App\Composer\Exception\ComposerNotFoundException;
use App\Exception\AppException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Output\StreamOutput;

class ComposerRunCommand extends BaseCommand
{
    protected static $defaultName = 'composer:run';

    protected function configure(): void
    {
        parent::configure();

        $this
            ->setDescription('Run composer and install packages.')
            ->addOption('dev', 'd', InputOption::VALUE_NONE, 'Install development packages')
            ->addOption('log', 'l', InputOption::VALUE_REQUIRED, 'File path to save composer logs.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $logFile = $input->getOption('log');
        if ($logFile) {
            $output = new StreamOutput(\fopen($logFile, 'a'));
        }

        $extraArgs = [];
        if (!$input->getOption('dev')) {
            $extraArgs[] = '--no-dev';
        }

        $workingDir = $this->getApplication()->getRootDir().'/vendor_bundled';

        try {
            $composer = $this->getService('Composer');
            $composer->setOutput($output);
            $exitCode = $composer->installDependencies($workingDir, $extraArgs);
        } catch (ComposerNotFoundException $e) {
            throw new AppException('Composer could not be found. It can be installed using: <info>setup.phar composer:download.</info>');
        }

        if (1 === $exitCode) {
            throw new AppException('Error installing composer packages');
        }

        $this->io->success('Tiki packages installed.');

        return $exitCode;
    }
}
