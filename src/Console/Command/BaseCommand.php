<?php

declare(strict_types=1);

namespace App\Console\Command;

use App\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class BaseCommand extends Command
{
    /**
     * @var SymfonyStyle
     */
    protected $io;

    /**
     * @var ContainerBuilder
     */
    protected $container;

    /**
     * BaseCommand constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return Application
     */
    public function getApplication()
    {
        return parent::getApplication();
    }

    public function isCompatible()
    {
        return true;
    }

    protected function configure(): void
    {
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $this->io = new SymfonyStyle($input, $output);

        $configFile = null;
        $application = $this->getApplication();

        $application->loadConfiguration($configFile);

        $this->container = $application->getContainer();
    }

    protected function getService(string $id)
    {
        return $this->container->get($id);
    }

    protected function getParameter(string $id)
    {
        return $this->getService(ParameterBagInterface::class)->get($id);
    }
}
