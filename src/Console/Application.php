<?php

declare(strict_types=1);

namespace App\Console;

use App\Utils;
use Psr\Container\ContainerInterface;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Console\Application as BaseApplication;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Filesystem\Filesystem;

class Application extends BaseApplication
{
    use Utils;

    public const CONFIG_DIST_FILENAME = 'config/config.yaml';

    protected $logo = '
88888888888 8888888 888    d8P  8888888       .d8888b.           888
    888       888   888   d8P     888        d88P  Y88b          888
    888       888   888  d8P      888        Y88b.               888
    888       888   888d88K       888         "Y888b.    .d88b.  888888 888  888 88888b.
    888       888   8888888b      888            "Y88b. d8P  Y8b 888    888  888 888 "88b
    888       888   888  Y88b     888              "888 88888888 888    888  888 888  888
    888       888   888   Y88b    888        Y88b  d88P Y8b.     Y88b.  Y88b 888 888 d88P
    888     8888888 888    Y88b 8888888       "Y8888P"   "Y8888   "Y888  "Y88888 88888P"
                                                                                 888
                                                                                 888
                                                                                 888
';

    /**
     * @var string
     */
    private $rootDir;

    /**
     * @var ContainerInterface|null
     */
    private $container;

    public function __construct(string $rootDir)
    {
        $this->rootDir = $rootDir;

        $container = $this->getContainer();
        $name = $container->getParameter('app.name');
        $version = $container->getParameter('app.version');

        parent::__construct($name, $version);
    }

    public function getVersion(): string
    {
        return 'version: '.parent::getVersion();
    }

    public function getName(): string
    {
        $name = parent::getName();

        return $name.PHP_EOL.$this->logo;
    }

    public function loadConfiguration(?string $configFile): void
    {
        if (null !== $this->container) {
            return;
        }

        $this->container = new ContainerBuilder();
        $this->container->setParameter('working_dir', $this->rootDir);

        try {
            $loader = new YamlFileLoader($this->container, new FileLocator(\dirname(__DIR__, 2)));
            $loader->load(static::CONFIG_DIST_FILENAME);
        } catch (\Exception $e) {
            throw new \LogicException(\sprintf('Load configuration failed "%s"', $e->getMessage()), $e->getCode(), $e);
        }

        $this->container->compile();
    }

    public function getContainer(): ContainerInterface
    {
        if (null === $this->container) {
            $this->loadConfiguration(null);
        }

        return $this->container;
    }

    public function getRootDir(): string
    {
        return $this->rootDir;
    }

    public function getTempDir(): string
    {
        $fileSystem = new Filesystem();
        $fileSystem->mkdir($this->rootDir.'/temp');

        return $this->rootDir.'/temp';
    }

    protected function getDefaultCommands(): array
    {
        return \array_merge(parent::getDefaultCommands(), [
            new Command\PermissionFixCommand(),
            new Command\PermissionOpenCommand(),
            new Command\ComposerRunCommand(),
            new Command\ComposerDownloadCommand(),
            new Command\PermissionAdvanceCommand(),
        ]);
    }

    protected function getDefaultInputDefinition()
    {
        return new InputDefinition([
            new InputArgument('command', InputArgument::REQUIRED, 'The command to execute'),
            new InputOption('--help', '-h', InputOption::VALUE_NONE, 'Display this help message'),
            new InputOption('--quiet', '-q', InputOption::VALUE_NONE, 'Do not output any message'),
            new InputOption('--verbose', '-v|vv|vvv', InputOption::VALUE_NONE, 'Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug'),
            new InputOption('--version', '-V', InputOption::VALUE_NONE, 'Display this application version'),
            new InputOption('--ansi', '', InputOption::VALUE_NONE, 'Force ANSI output'),
            new InputOption('--no-ansi', '', InputOption::VALUE_NONE, 'Disable ANSI output'),
            new InputOption('--no-interaction', '-n', InputOption::VALUE_NONE, 'Do not ask any interactive question'),
        ]);
    }
}
