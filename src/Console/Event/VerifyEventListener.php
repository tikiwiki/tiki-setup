<?php

declare(strict_types=1);

namespace App\Console\Event;

use App\Console\Command\BaseCommand;
use App\Exception\AppException;
use Symfony\Component\Console\ConsoleEvents;
use Symfony\Component\Console\Event\ConsoleCommandEvent;

class VerifyEventListener implements AppEventListener
{
    public function register($dispatcher): void
    {
        $dispatcher->addListener(ConsoleEvents::COMMAND, function (ConsoleCommandEvent $event): void {
            $command = $event->getCommand();
            if ($command instanceof BaseCommand) {
                $rootDir = $command->getApplication()->getRootDir();
                if (!\file_exists($rootDir.'/tiki-index.php')) {
                    throw new AppException('Tiki application not found. Please copy setup.phar to a Tiki\'s root folder.');
                }

                $minPHP = '7.2';
                if (-1 === \version_compare(\phpversion(), $minPHP)) {
                    throw new AppException(\sprintf('PHP %s or greater is required.', $minPHP));
                }

                if (!$command->isCompatible()) {
                    throw new AppException('This command is not compatible with this operative system');
                }
            }
        });
    }
}
