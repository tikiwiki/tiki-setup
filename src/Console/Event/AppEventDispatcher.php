<?php

declare(strict_types=1);

namespace App\Console\Event;

use Symfony\Component\EventDispatcher\EventDispatcher;

class AppEventDispatcher extends EventDispatcher
{
    public function __construct()
    {
        parent::__construct();
        $this->loadListeners();
    }

    private function loadListeners(): void
    {
        $listeners = [
            new ErrorEventListener(),
            new VerifyEventListener(),
        ];

        foreach ($listeners as $listener) {
            $listener->register($this);
        }
    }
}
