<?php

declare(strict_types=1);

namespace App;

use App\Permission\DefaultUserPermission;
use Symfony\Component\Process\Process;

trait Utils
{
    public function getUserGroups(string $currentUser, $timeout = 900)
    {
        $process = new Process(['id', ' -Gn ', $currentUser]);
        $process->setTimeout($timeout);
        $process->run();
        if (0 === $process->getExitCode()) {
            return \explode(' ', $process->getOutput());
        }

        return [];
    }

    public function isUserInGroup($user, $group)
    {
        $groups = $this->getUserGroups($user);

        return \in_array($group, $groups, true);
    }

    public function getCurrentUser()
    {
        return \get_current_user();
    }

    public function isRoot()
    {
        return 0 === \posix_getuid();
    }

    public function verifyCommand($command)
    {
        $windows = 0 === \strpos(PHP_OS, 'WIN');
        $test = $windows ? 'where' : 'command -v';
        $path = \shell_exec("$test $command");
        if (!$path) {
            return false;
        }
        $path = \trim($path);
        if (\file_exists($path) && \is_executable($path)) {
            return $path;
        }

        return false;
    }

    /**
     * @return DefaultUserPermission
     */
    public function getDefaultUserGroupPermission()
    {
        if (\file_exists('/etc/debian_version')) {
            return new DefaultUserPermission('www-data', 'www-data');
        } elseif (\file_exists('/etc/redhat-release') || \file_exists('/etc/gentoo-release')) {
            return new DefaultUserPermission('apache', 'apache');
        } elseif (\file_exists('/etc/SuSE-release')) {
            return new DefaultUserPermission('wwwrun', 'wwwrun');
        } elseif (false !== \strpos(\strtolower(PHP_OS), 'cygwin')) {
            return new DefaultUserPermission('SYSTEM', 'SYSTEM');
        } elseif (false !== \strpos(\strtolower(PHP_OS), 'darwin')) {
            return new DefaultUserPermission('_www', '_www');
        }

        return new DefaultUserPermission('www', 'www');
    }
}
