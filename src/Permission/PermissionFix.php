<?php

declare(strict_types=1);

namespace App\Permission;

use App\FileSystem\UnixFileSystem;
use App\Utils;
use Symfony\Component\Console\Output\OutputInterface;

class PermissionFix extends Permission
{
    use Utils;

    private $group;

    private $multi;

    private $user;

    /**
     * PermissionFix constructor.
     *
     * @param $workingDir
     * @param $user
     * @param $group
     * @param array           $multi
     * @param OutputInterface $output
     */
    public function __construct($workingDir, $user, $group, $multi = [], $output = null)
    {
        parent::__construct($workingDir, $output, new UnixFileSystem());
        $this->group = $group;
        $this->multi = $multi;
        $this->output = $output;
        $this->user = $user;
    }

    public function apply(): void
    {
        $this->updateMultiVirtual();
        $this->checkComposerNonBundled();
        $this->checkDirectories();
        $this->applyOwnerGroup();
        $this->applyPermissions();
    }

    private function updateMultiVirtual(): void
    {
        $multiVirtual = $this->getMultiVirtual();
        $multiVirtual = \array_merge($multiVirtual, $this->multi);
        $multiVirtual = \array_unique($multiVirtual);
        if (\file_exists($this->getMultiVirtualFile())) {
            $this->getFileSystem()->remove($this->getMultiVirtualFile());
        }
        $this->getFileSystem()->appendToFile($this->getMultiVirtualFile(), \implode(PHP_EOL, $multiVirtual));
    }

    private function checkComposerNonBundled(): void
    {
        // make sure composer files can be created by tiki-admin.php?page=packages
        $this->log('Checking non-bundled composer : ');
        if (!\file_exists($this->workingDir.'/composer.json')) {
            $this->log('Creating composer.json...');
            $this->getFileSystem()->copy(
                $this->workingDir.'/composer.json.dist',
                $this->workingDir.'/composer.json'
            );
        } else {
            $this->log('Found composer.json...');
        }

        if (!\file_exists($this->workingDir.'/composer.lock')) {
            $this->log('Created composer.lock...');
            $this->getFileSystem()->appendToFile(
                $this->workingDir.'/composer.lock',
                '{}'
            );
        } else {
            $this->log('Found composer.lock...');
        }
    }

    private function checkDirectories(): void
    {
        $virtuals = $this->getMultiVirtual();
        $this->log('Checking dirs :');
        foreach (self::DIR_LIST_WRITABLE as $dir) {
            $this->log($dir.'...', OutputInterface::VERBOSITY_VERBOSE);
            if (!$this->getFileSystem()->exists($this->workingDir.DIRECTORY_SEPARATOR.$dir)) {
                $this->log('Creating directory', OutputInterface::VERBOSITY_VERBOSE);
                $this->getFileSystem()->mkdir($this->workingDir.DIRECTORY_SEPARATOR.$dir);
            }
            $this->log('ok', OutputInterface::VERBOSITY_VERBOSE);
            if (!empty($virtuals) && 'temp/unified-index' !== $dir) {
                foreach ($virtuals as $virtual) {
                    $this->log($dir.DIRECTORY_SEPARATOR.$virtual, OutputInterface::VERBOSITY_VERBOSE);
                    if (!\file_exists($this->workingDir.DIRECTORY_SEPARATOR
                        .$dir.DIRECTORY_SEPARATOR.$virtual)) {
                        $this->log('Creating directory', OutputInterface::VERBOSITY_VERBOSE);
                        $this->getFileSystem()->mkdir($this->workingDir.DIRECTORY_SEPARATOR.$dir.DIRECTORY_SEPARATOR.$virtual);
                    }
                    $this->log('ok', OutputInterface::VERBOSITY_VERBOSE);
                }
            }
        }
    }

    private function getMultiVirtualFile()
    {
        return \implode(DIRECTORY_SEPARATOR, [$this->workingDir, 'db', 'virtuals.inc']);
    }

    private function getMultiVirtual()
    {
        if (\file_exists($this->getMultiVirtualFile())) {
            return \explode(PHP_EOL, \file_get_contents($this->getMultiVirtualFile()));
        }

        return [];
    }

    private function applyOwnerGroup(): void
    {
        $aUser = $this->user;
        $aGroup = $this->group;

        $currentUser = $this->getCurrentUser();

        $this->log('Fix global perms ...');
        if ($this->isRoot()) {
            $this->log(\sprintf('Change user to %s and group to %s...', $aUser, $aGroup));
            @$this->getFileSystem()->chown($this->workingDir, $aUser, true);
            @$this->getFileSystem()->chgrp($this->workingDir, $aGroup, true);
            $this->log('done');

            return;
        }

        $this->log('You are not root. We will not try to change the file owners.', OutputInterface::VERBOSITY_VERBOSE);

        if ($this->isUserInGroup($currentUser, $aGroup)) {
            $this->log('Change group to '.$aGroup.' ...');
            @$this->getFileSystem()->chgrp($this->workingDir, $aGroup);

            return;
        }

        $this->log(
            'You are not root and you are not in the group '.$aGroup.'. We can\'t change the group ownership to '.$aGroup.'.',
            OutputInterface::VERBOSITY_VERBOSE
        );
        $this->log('Special dirs permissions will be set accordingly.');
    }

    private function applyPermissions(): void
    {
        $aGroup = $this->group;

        $currentUser = $this->getCurrentUser();

        $this->log('Fix normal dirs ...');
        // TODO join with old permission
        $message = null;
        @$this->getFileSystem()->chmodOS($this->workingDir, 'u=rwX,go=rX', $message);
        $this->log($message, OutputInterface::VERBOSITY_VERBOSE);

        $this->log('done', OutputInterface::VERBOSITY_VERBOSE);
        $this->log('Fix special dirs ...');

        $perm = ($this->isRoot() || $this->isUserInGroup($currentUser, $aGroup)) ? 'g+w' : 'go+w';
        foreach (self::DIR_LIST_WRITABLE as $dir) {
            @$this->getFileSystem()->chmodOS($this->workingDir.DIRECTORY_SEPARATOR.$dir, $perm, $message);
            $this->log($message, OutputInterface::VERBOSITY_VERBOSE);
        }

        @$this->getFileSystem()->chmodOS($this->workingDir.'/composer.json', $perm, $message);
        $this->log($message, OutputInterface::VERBOSITY_VERBOSE);
        @$this->getFileSystem()->chmodOS($this->workingDir.'/composer.lock', $perm, $message);
        $this->log($message, OutputInterface::VERBOSITY_VERBOSE);
    }
}
