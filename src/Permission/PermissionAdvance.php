<?php

declare(strict_types=1);

namespace App\Permission;

use App\FileSystem\UnixFileSystem;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;

class PermissionAdvance extends Permission
{
    private $permSetting;

    /**
     * PermissionAdvance constructor.
     *
     * @param PermissionSetting $permSetting
     * @param $workingDir
     * @param $output
     */
    public function __construct($permSetting, $workingDir, $output)
    {
        parent::__construct($workingDir, $output, new UnixFileSystem());
        $this->permSetting = $permSetting;
    }

    public static function getPermSettings()
    {
        return [
            new PermissionSetting('paranoia', 0700, 0600, 0700, 0600),
            new PermissionSetting('paranoia-suphp', 0701, 0600, 0701, 0600),
            new PermissionSetting('sbox', 0501, 0501, 0701, 0701),
            new PermissionSetting('worry', 0701, 0604, 0703, 0606),
            new PermissionSetting('moreworry', 0705, 0604, 0707, 0606),
            new PermissionSetting('pain', 0701, 0606, 0703, 0606),
            new PermissionSetting('morepain', 0705, 0606, 0707, 0606),
            new PermissionSetting('mixed', 0770, 0660, 0770, 0660),
            new PermissionSetting('risky', 0775, 0664, 0777, 0666),
            new PermissionSetting('insane', 0777, 0777, 0777, 0777),
        ];
    }

    public static function getPermsNames()
    {
        return \array_map(function ($permissionSetting) {
            return $permissionSetting->getName();
        }, self::getPermSettings());
    }

    public static function getPermissionSetting($name)
    {
        $perms = \array_values(\array_filter(self::getPermSettings(), function ($permissionSetting) use ($name) {
            return $permissionSetting->getName() === $name;
        }));

        return empty($perms) ? false : $perms[0];
    }

    public function apply(): void
    {
        $folders = $this->preparePaths(\array_merge(self::DIR_LIST_DEFAULT, self::DIR_LIST_WRITABLE));
        $finder = new Finder();
        $finder->files()->in($folders);

        //Apply permissions to PHP files
        $finder = new Finder();

        $finder->files()->depth(0)->name('*.php')->in($this->workingDir);
        foreach ($finder as $file) {
            $this->log('apply permission to '.$file.' -> '.\decoct($this->permSetting->getPermsFiles()), OutputInterface::VERBOSITY_VERBOSE);
            $this->getFileSystem()->chmod($file->getPathname(), $this->permSetting->getPermsFiles());
        }
        $this->getFileSystem()->chmod($this->workingDir, $this->permSetting->getPermsSubDir());

        $folders = $this->preparePaths(self::DIR_LIST_DEFAULT);
        foreach ($folders as $folder) {
            $this->getFileSystem()->chmod($folder, $this->permSetting->getPermsSubDir());
            $finder = new Finder();
            $finder->directories()->in($folder);
            foreach ($finder as $file) {
                $this->log('apply directories permission to '.$file.' -> '.\decoct($this->permSetting->getPermsSubDir()), OutputInterface::VERBOSITY_VERBOSE);
                $this->getFileSystem()->chmod($file->getPathname(), $this->permSetting->getPermsSubDir());
            }
            $finder = new Finder();
            $finder->files()->in($folder);
            foreach ($finder as $file) {
                $this->log('apply permission to '.$file.' -> '.\decoct($this->permSetting->getPermsFiles()), OutputInterface::VERBOSITY_VERBOSE);
                $this->getFileSystem()->chmod($file->getPathname(), $this->permSetting->getPermsFiles());
            }
        }

        $folders = $this->preparePaths(self::DIR_LIST_WRITABLE);
        foreach ($folders as $folder) {
            $this->getFileSystem()->chmod($folder, $this->permSetting->getPermsWriteSubDirs());
            $finder = new Finder();
            $finder->files()->in($folder);
            foreach ($finder as $file) {
                $this->log('apply permission to '.$file.' -> '.\decoct($this->permSetting->getPermsWriteFiles()), OutputInterface::VERBOSITY_VERBOSE);
                $this->getFileSystem()->chmod($file->getPathname(), $this->permSetting->getPermsWriteFiles());
            }

            $finder = new Finder();
            $finder->directories()->in($folder);
            foreach ($finder as $file) {
                $this->log('apply directories permission to '.$file.' -> '.\decoct($this->permSetting->getPermsWriteSubDirs()), OutputInterface::VERBOSITY_VERBOSE);
                $this->getFileSystem()->chmod($file->getPathname(), $this->permSetting->getPermsWriteSubDirs());
            }
        }
    }

    private function preparePaths($paths = [])
    {
        return \array_filter(\array_map(function ($folder) {
            return $this->workingDir.DIRECTORY_SEPARATOR.$folder;
        }, $paths), function ($path) {
            return \file_exists($path);
        });
    }
}
