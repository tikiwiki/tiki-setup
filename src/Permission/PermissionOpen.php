<?php

declare(strict_types=1);

namespace App\Permission;

use App\FileSystem\UnixFileSystem;
use App\Utils;
use Symfony\Component\Console\Output\OutputInterface;

class PermissionOpen extends Permission
{
    use Utils;

    private $user;

    /**
     * PermissionFix constructor.
     *
     * @param $workingDir
     * @param $user
     * @param OutputInterface $output
     */
    public function __construct($workingDir, $user, $output = null)
    {
        parent::__construct($workingDir, $output, new UnixFileSystem());
        $this->output = $output;
        $this->user = $user;
    }

    public function apply(): void
    {
        if ($this->isRoot()) {
            $this->output->writeln('Changing owner to '.$this->user);
            $this->getFileSystem()->chown($this->workingDir, $this->user, true);
        }
        $this->output->writeln('Applying permissions');
        $this->getFileSystem()->chmod($this->workingDir, 0777, 0000, true);
    }
}
