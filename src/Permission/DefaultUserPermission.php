<?php

declare(strict_types=1);

namespace App\Permission;

class DefaultUserPermission
{
    private $auser;

    private $agroup;

    /**
     * DefaultUserPermission constructor.
     *
     * @param $auser
     * @param $agroup
     */
    public function __construct($auser, $agroup)
    {
        $this->auser = $auser;
        $this->agroup = $agroup;
    }

    /**
     * @return mixed
     */
    public function getAuser()
    {
        return $this->auser;
    }

    /**
     * @return mixed
     */
    public function getAgroup()
    {
        return $this->agroup;
    }
}
