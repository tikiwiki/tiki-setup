<?php

declare(strict_types=1);

namespace App\Permission;

class PermissionSetting
{
    /**
     * @var
     */
    private $name;

    private $permsSubDir;

    private $permsFiles;

    private $permsWriteSubDirs;

    private $permsWriteFiles;

    /**
     * PermissionSetting constructor.
     *
     * @param $name
     * @param $permsSubDir
     * @param $permsFiles
     * @param $permsWriteSubDirs
     * @param $permsWriteFiles
     */
    public function __construct($name, $permsSubDir, $permsFiles, $permsWriteSubDirs, $permsWriteFiles)
    {
        $this->name = $name;
        $this->permsSubDir = $permsSubDir;
        $this->permsFiles = $permsFiles;
        $this->permsWriteSubDirs = $permsWriteSubDirs;
        $this->permsWriteFiles = $permsWriteFiles;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPermsSubDir()
    {
        return $this->permsSubDir;
    }

    /**
     * @param mixed $permsSubDir
     */
    public function setPermsSubDir($permsSubDir): void
    {
        $this->permsSubDir = $permsSubDir;
    }

    /**
     * @return mixed
     */
    public function getPermsFiles()
    {
        return $this->permsFiles;
    }

    /**
     * @param mixed $permsFiles
     */
    public function setPermsFiles($permsFiles): void
    {
        $this->permsFiles = $permsFiles;
    }

    /**
     * @return mixed
     */
    public function getPermsWriteSubDirs()
    {
        return $this->permsWriteSubDirs;
    }

    /**
     * @param mixed $permsWriteSubDirs
     */
    public function setPermsWriteSubDirs($permsWriteSubDirs): void
    {
        $this->permsWriteSubDirs = $permsWriteSubDirs;
    }

    /**
     * @return mixed
     */
    public function getPermsWriteFiles()
    {
        return $this->permsWriteFiles;
    }

    /**
     * @param mixed $permsWriteFiles
     */
    public function setPermsWriteFiles($permsWriteFiles): void
    {
        $this->permsWriteFiles = $permsWriteFiles;
    }
}
