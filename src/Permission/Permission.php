<?php

declare(strict_types=1);

namespace App\Permission;

use App\FileSystem\CustomFileSystem;
use Symfony\Component\Console\Output\OutputInterface;

abstract class Permission
{
    public const DIR_LIST_WRITABLE = [
        'db', 'dump', 'img/wiki', 'img/wiki_up', 'img/trackers',
        'modules/cache', 'storage', 'storage/public', 'temp', 'temp/cache',
        'temp/public', 'temp/templates_c', 'templates', 'themes', 'whelp', 'mods',
        'files', 'tiki_tests/tests', 'temp/unified-index', 'vendor',
    ];

    public const DIR_LIST_DEFAULT = [
        'admin', 'db', 'doc', 'dump', 'files', 'img', 'installer', 'lang',
        'lib', 'modules', 'permissioncheck', 'storage', 'temp', 'templates',
        'tests', 'themes', 'tiki_tests', 'vendor', 'vendor_extra', 'whelp',
    ];

    protected $workingDir;

    /**
     * @var OutputInterface
     */
    protected $output;

    private $fileSystem;

    /**
     * Permission constructor.
     *
     * @param $workingDir
     * @param null $fileSystem
     */
    public function __construct($workingDir, OutputInterface $output, $fileSystem = null)
    {
        $this->output = $output;
        $this->fileSystem = $fileSystem;
        $this->workingDir = $workingDir;
    }

    abstract public function apply();

    /**
     * @return CustomFileSystem
     */
    public function getFileSystem()
    {
        return $this->fileSystem;
    }

    public function log($message, $options = 0): void
    {
        if ($message) {
            $this->output->writeln($message, $options);
        }
    }
}
