<?php

declare(strict_types=1);

namespace Tests\App\Composer;

use App\Composer\Composer;
use App\Composer\Exception\ComposerDownloadException;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Filesystem\Filesystem;

class ComposerTest extends TestCase
{
    private static $workingDir;

    private $output;

    private $composer;

    public function setUp(): void
    {
        self::$workingDir = \sys_get_temp_dir().'/test';
        $fileSystem = new Filesystem();
        $fileSystem->mkdir(self::$workingDir);

        $this->output = new BufferedOutput();
        $this->composer = new Composer(self::$workingDir, $this->output);
        $this->composer->setInstallerURL('https://getcomposer.org/installer');
        $this->composer->setInstallerSignatureUrl('https://composer.github.io/installer.sig');
    }

    protected function tearDown(): void
    {
        $fileSystem = new Filesystem();
        $fileSystem->remove(self::$workingDir);
    }

    public function testDownload(): void
    {
        $this->composer->download();
        $this->assertFileExists(self::$workingDir.'/composer.phar');
    }

    public function testDownloadInvalidSignature(): void
    {
        $invalidSigPath = self::$workingDir.'/invalid.sig';
        \file_put_contents($invalidSigPath, '123');
        $this->expectException(ComposerDownloadException::class);

        $this->composer->setInstallerSignatureUrl($invalidSigPath);
        $this->composer->download();
    }

    public function testDownloadInvalidPath(): void
    {
        $invalidInstallerPath = self::$workingDir.'/installer';
        $invalidSigPath = self::$workingDir.'/invalid.sig';
        \file_put_contents($invalidSigPath, '123');
        $this->expectException(ComposerDownloadException::class);

        $this->composer->setInstallerURL($invalidInstallerPath);
        $this->composer->setInstallerSignatureUrl($invalidSigPath);
        $this->composer->download();
    }

    public function testInstallDependencies(): void
    {
        $this->assertFileNotExists(self::$workingDir.'/vendor');

        $composerFile = [
            'require' => [
                'vlucas/phpdotenv' => '^4.1',
            ],
        ];

        $fileSystem = new Filesystem();
        $fileSystem->appendToFile(self::$workingDir.'/composer.json', \json_encode($composerFile));
        $this->assertFileExists(self::$workingDir.'/composer.json');

        $exitCode = $this->composer->installDependencies(self::$workingDir);

        $this->assertEquals(0, $exitCode);
        $this->assertFileExists(self::$workingDir.'/vendor');
        $this->assertFileExists(self::$workingDir.'/composer.lock');
    }
}
