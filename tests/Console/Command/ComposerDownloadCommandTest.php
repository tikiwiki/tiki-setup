<?php

declare(strict_types=1);

namespace Tests\App\Console\Command;

use App\Console\Application;
use App\Console\Command\ComposerDownloadCommand;
use Symfony\Component\Console\Tester\CommandTester;
use Tests\App\CommandTestCase;

class ComposerDownloadCommandTest extends CommandTestCase
{
    private static $workingDir;

    public function setUp(): void
    {
        self::$workingDir = \sys_get_temp_dir().'/test';
    }

    public function testComposerDownloadCommand(): void
    {
        $application = new Application(\dirname(__DIR__, 3));
        $application->add(new ComposerDownloadCommand());
        $command = $application->find('composer:download');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command' => $command->getName(),
        ]);

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('Composer downloaded successfully', $output);
        $this->assertFileExists($application->getTempDir().'/composer.phar');
    }
}
