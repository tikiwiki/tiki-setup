<?php

declare(strict_types=1);

namespace App\Console\Command;

use App\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\Filesystem\Filesystem;
use Tests\App\CommandTestCase;

class ComposerRunCommandTest extends CommandTestCase
{
    /**
     * @var string
     */
    private static $workingDir;

    public function setUp(): void
    {
        self::$workingDir = \sys_get_temp_dir().'/test';

        $fileSystem = new Filesystem();
        $fileSystem->mkdir(self::$workingDir);
        $fileSystem->mkdir(self::$workingDir.'/vendor_bundled');

        $composerFile = [
            'require' => [
                'vlucas/phpdotenv' => '^4.1',
            ],
        ];

        $fileSystem->dumpFile(self::$workingDir.'/vendor_bundled/composer.json', \json_encode($composerFile));
        $fileSystem->dumpFile(self::$workingDir.'/tiki-index.php', '');
    }

    protected function tearDown(): void
    {
        $fileSystem = new Filesystem();
        $fileSystem->remove(self::$workingDir);
    }

    public function testComposerRunCommand(): void
    {
        $this->assertFalse(\file_exists(self::$workingDir.'/vendor_bundled/vendor'));

        $application = new Application(self::$workingDir);
        $application->add(new ComposerRunCommand());
        $command = $application->find('composer:run');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command' => $command->getName(),
        ]);

        $this->assertEquals(0, $commandTester->getStatusCode());
        $this->assertFileExists(self::$workingDir.'/vendor_bundled/vendor');
    }

    public function testComposerRunCommandLog(): void
    {
        $logFile = self::$workingDir.'/composer.log';

        $this->assertFileNotExists($logFile);
        $this->assertFalse(\file_exists(self::$workingDir.'/vendor_bundled/vendor'));

        $application = new Application(self::$workingDir);
        $application->add(new ComposerRunCommand());
        $command = $application->find('composer:run');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command' => $command->getName(),
            '-l' => $logFile,
        ]);

        $this->assertEquals(0, $commandTester->getStatusCode());
        $this->assertFileExists(self::$workingDir.'/vendor_bundled/vendor');
        $this->assertFileExists($logFile);
        $this->assertNotEmpty(\file_get_contents($logFile));
    }
}
