<?php

declare(strict_types=1);

namespace App\Permission;

use App\FileSystem\UnixFileSystem;
use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;

class PermissionFixTest extends TestCase
{
    /**
     * @var mixed
     */
    private static $directory;

    protected function setUp(): void
    {
    }

    public function testApply(): void
    {
        $fileSystem = vfsStream::setup(
            'root',
            null,
            [
                'composer.json.dist' => 'Some content',
                'db' => [
                    'local.php' => [],
                ],
            ]
        );
        $bufferOutput = new BufferedOutput();
        $fileSystemSy = $this->getMockBuilder(UnixFileSystem::class)
            ->onlyMethods(['chown', 'chgrp'])->getMock();
        $permission = $this->getMockBuilder(PermissionFix::class)
            ->setConstructorArgs([$fileSystem->url(), 'www', 'www', [], $bufferOutput])
            ->onlyMethods(['isRoot', 'getFileSystem'])->getMock();
        $permission->method('isRoot')->willReturn(true);
        $permission->method('getFileSystem')->willReturn($fileSystemSy);

        $permission->apply();

        //check $this->workingDir.DIRECTORY_SEPARATOR.'composer.json'
        $this->assertFileExists($fileSystem->url().DIRECTORY_SEPARATOR.'composer.json');
        $this->assertFileExists($fileSystem->url().DIRECTORY_SEPARATOR.'composer.lock');
        foreach (PermissionFix::DIR_LIST_WRITABLE as $dir) {
            $this->assertFileExists($fileSystem->url().DIRECTORY_SEPARATOR.$dir);
        }

        $output = $bufferOutput->fetch();
        $this->assertStringContainsString('Change user to www and group to www...', $output);
        $this->assertStringContainsString('Fix normal dirs ...', $output);
        $this->assertStringContainsString('Fix special dirs ...', $output);
    }

    public function testApplyNotRoot(): void
    {
        $fileSystem = vfsStream::setup(
            'root',
            null,
            [
                'composer.json.dist' => 'Some content',
                'db' => [
                    'local.php' => [],
                ],
            ]
        );
        $bufferOutput = new BufferedOutput();
        $bufferOutput->setVerbosity(OutputInterface::VERBOSITY_VERBOSE);
        $fileSystemSy = $this->getMockBuilder(UnixFileSystem::class)
            ->onlyMethods(['chown', 'chgrp', 'chmodOS'])->getMock();
        $permission = $this->getMockBuilder(PermissionFix::class)
            ->setConstructorArgs([$fileSystem->url(), 'www', 'www', [], $bufferOutput])
            ->onlyMethods(['isRoot', 'getFileSystem', 'isUserInGroup'])->getMock();
        $permission->method('isRoot')->willReturn(false);
        $permission->method('isUserInGroup')->willReturn(false);
        $permission->method('getFileSystem')->willReturn($fileSystemSy);

        $permission->apply();

        //check $this->workingDir.DIRECTORY_SEPARATOR.'composer.json'
        $this->assertFileExists($fileSystem->url().DIRECTORY_SEPARATOR.'composer.json');
        $this->assertFileExists($fileSystem->url().DIRECTORY_SEPARATOR.'composer.lock');
        foreach (PermissionFix::DIR_LIST_WRITABLE as $dir) {
            $this->assertFileExists($fileSystem->url().DIRECTORY_SEPARATOR.$dir);
        }

        $output = $bufferOutput->fetch();
        $this->assertStringContainsString('You are not root. We will not try to change the file owners.', $output);
        $this->assertStringContainsString('You are not root and you are not in the group www. We can\'t change the group ownership to www.', $output);
        $this->assertStringContainsString('Fix normal dirs ...', $output);
        $this->assertStringContainsString('Fix special dirs ...', $output);
    }

    public function testApplyInGroupAndVirtual(): void
    {
        $fileSystem = vfsStream::setup(
            'root',
            null,
            [
                'composer.json.dist' => 'Some content',
                'db' => [
                    'virtuals.inc' => 'test.com',
                    'local.php' => [],
                ],
            ]
        );
        $bufferOutput = new BufferedOutput();
        $bufferOutput->setVerbosity(OutputInterface::VERBOSITY_VERBOSE);
        $fileSystemSy = $this->getMockBuilder(UnixFileSystem::class)
            ->onlyMethods(['chown', 'chgrp', 'chmodOS'])->getMock();
        $permission = $this->getMockBuilder(PermissionFix::class)
            ->setConstructorArgs([$fileSystem->url(), 'www', 'www', ['test1.com', 'test2.com'], $bufferOutput])
            ->onlyMethods(['isRoot', 'getFileSystem', 'isUserInGroup'])->getMock();
        $permission->method('isRoot')->willReturn(false);
        $permission->method('isUserInGroup')->willReturn(true);
        $permission->method('getFileSystem')->willReturn($fileSystemSy);

        $permission->apply();

        $virtualFinal = ['test.com', 'test1.com', 'test2.com'];

        //Test virtual applied
        $this->assertStringEqualsFile(
            $fileSystem->url().'/db/virtuals.inc',
            \implode(PHP_EOL, $virtualFinal)
        );

        //check $this->workingDir.DIRECTORY_SEPARATOR.'composer.json'
        $this->assertFileExists($fileSystem->url().DIRECTORY_SEPARATOR.'composer.json');
        $this->assertFileExists($fileSystem->url().DIRECTORY_SEPARATOR.'composer.lock');
        foreach (PermissionFix::DIR_LIST_WRITABLE as $dir) {
            $this->assertFileExists($fileSystem->url().DIRECTORY_SEPARATOR.$dir);
            if (!empty($virtuals) && 'temp/unified-index' !== $dir) {
                foreach ($virtualFinal as $item) {
                    $this->assertFileExists($fileSystem->url()
                        .DIRECTORY_SEPARATOR.$dir.DIRECTORY_SEPARATOR.$item);
                }
            }
        }

        $output = $bufferOutput->fetch();
        $this->assertStringContainsString('You are not root. We will not try to change the file owners.', $output);
        $this->assertStringContainsString('Change group to www ...', $output);
        $this->assertStringContainsString('Fix normal dirs ...', $output);
        $this->assertStringContainsString('Fix special dirs ...', $output);
    }
}
