<?php

declare(strict_types=1);

namespace App\Permission;

use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Finder\Finder;

class PermissionAdvanceTest extends TestCase
{
    public function testApply(): void
    {
        $fileSystem = vfsStream::setup(
            'root',
            0755,
            [
                'index.php' => 'content',
                'composer.json.dist' => 'Some content',
                'db' => [
                    'virtuals.inc' => 'test.com',
                    'local.php' => [],
                ],
                'dump' => [], 'img' => ['wiki_up' => [], 'wiki' => [], 'trackers' => []],
                'modules' => ['cache' => []], 'storage' => ['public' => []],
                'templates' => [], 'themes' => [], 'whelp' => [], 'mods' => [],
                'files' => [], 'tiki_tests' => ['tests' => []], 'temp' => ['unified-index' => [], 'cache' => [], 'templates_c' => [], 'public' => []], 'vendor' => [],
                'admin' => [], 'doc' => [], 'installer' => [], 'lang' => [],
                'lib' => [], 'permissioncheck' => [],
                'tests' => [], 'vendor_extra' => [],
            ]
        );
//        foreach (PermissionAdvance::getPermSettings() as $permSetting) {

        $permSetting = PermissionAdvance::getPermissionSetting('insane');
        $this->assertNotEquals(\decoct($permSetting->getPermsSubDir()), \decoct(\fileperms($fileSystem->url()) & 0777));
        $output = new BufferedOutput();
        $permission = new PermissionAdvance($permSetting, $fileSystem->url(), $output);
        $permission->apply();

        //Apply permissions to PHP files
        $finder = new Finder();
        $finder->name('*.php')->depth(0)->in($fileSystem->url());
        foreach ($finder as $file) {
            $this->assertEquals(\decoct($permSetting->getPermsFiles()), \decoct(\fileperms($file->getPathname()) & 0777));
        }

        $this->assertEquals(\decoct($permSetting->getPermsSubDir()), \decoct(\fileperms($fileSystem->url()) & 0777));

        $finder = new Finder();
        $finder->files()->in(\array_map(function ($folder) use ($fileSystem, $permSetting) {
            return $fileSystem->url().DIRECTORY_SEPARATOR.$folder;
        }, Permission::DIR_LIST_DEFAULT));
        foreach ($finder as $file) {
            $this->assertEquals(\decoct($permSetting->getPermsFiles()), \decoct(\fileperms($file->getPathname()) & 0777), $permSetting->getName());
        }

        $finder = new Finder();
        $finder->directories()->in(\array_map(function ($folder) use ($fileSystem, $permSetting) {
            return $fileSystem->url().DIRECTORY_SEPARATOR.$folder;
        }, Permission::DIR_LIST_DEFAULT));
        foreach ($finder as $file) {
            $this->assertEquals(\decoct($permSetting->getPermsSubDir()), \decoct(\fileperms($file->getPathname()) & 0777), $permSetting->getName());
        }

        $finder = new Finder();
        $finder->files()->in(\array_map(function ($folder) use ($fileSystem, $permSetting) {
            return $fileSystem->url().DIRECTORY_SEPARATOR.$folder;
        }, Permission::DIR_LIST_WRITABLE));
        foreach ($finder as $file) {
            $this->assertEquals(\decoct($permSetting->getPermsWriteFiles()), \decoct(\fileperms($file->getPathname()) & 0777));
        }

        $finder = new Finder();
        $finder->directories()->in(\array_map(function ($folder) use ($fileSystem, $permSetting) {
            return $fileSystem->url().DIRECTORY_SEPARATOR.$folder;
        }, Permission::DIR_LIST_WRITABLE));
        foreach ($finder as $file) {
            $this->assertEquals(\decoct($permSetting->getPermsWriteSubDirs()), \decoct(\fileperms($file->getPathname()) & 0777));
        }
//        }
    }
}
