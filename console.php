<?php

declare(strict_types=1);

require __DIR__.'/vendor/autoload.php';

use App\Console\Application;
use App\Console\Event\AppEventDispatcher;

\set_time_limit(0);
$workingDir = \dirname(Phar::running(false));
if (!$workingDir) {
    $workingDir = __DIR__;
}

$app = new Application($workingDir);
$app->setDispatcher(new AppEventDispatcher());
$app->run();
