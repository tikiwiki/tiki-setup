image: xorti/tiki-devops:7.2-qa-box

variables:
    COMPOSER_CACHE_DIR: "$CI_PROJECT_DIR/.composercache"
    COMPOSER_FLAGS: '--ansi --no-progress --prefer-dist -n'

# Cache libraries in between jobs
.cache: &cache
    key: cache-tiki-setup
    paths:
        - vendor/
        - .composercache/files

stages:
    - build
    - tests
    - quality
    - package

#----------------------------------------------------------------------------------------------------------------------#
# BUILD SECTION
#----------------------------------------------------------------------------------------------------------------------#

composer:
  stage: build
  script:
    - composer install $COMPOSER_FLAGS
  cache:
    <<: *cache

#----------------------------------------------------------------------------------------------------------------------#
# TESTS SECTION
#----------------------------------------------------------------------------------------------------------------------#
unit-tests:
    stage: tests
    script:
        - composer install $COMPOSER_FLAGS
        - ./vendor/bin/phpunit tests/ --coverage-text --colors=never --configuration phpunit.xml.dist
    needs:
        - composer
    allow_failure: false
    cache:
        <<: *cache
        policy: pull

#
# Lint
#
phpcs:
    stage: quality
    script:
        - composer install $COMPOSER_FLAGS
        - vendor/bin/php-cs-fixer fix --dry-run
    needs:
        - composer
    allow_failure: false
    cache:
        <<: *cache
        policy: pull

#----------------------------------------------------------------------------------------------------------------------#
# QUALITY SECTION
#----------------------------------------------------------------------------------------------------------------------#

code_quality:
    image: docker:stable
    stage: quality
    variables:
        DOCKER_DRIVER: overlay2
        SP_VERSION: 0.85.6
    allow_failure: true
    services:
        - docker:stable-dind
    script:
        - docker run
            --env SOURCE_CODE="$PWD"
            --volume "$PWD":/code
            --volume /var/run/docker.sock:/var/run/docker.sock
            "registry.gitlab.com/gitlab-org/security-products/codequality:$SP_VERSION" /code
    artifacts:
        reports:
            codequality: gl-code-quality-report.json
    needs: []
    tags:
        - docker



#----------------------------------------------------------------------------------------------------------------------#
# PACKAGE SECTION
#----------------------------------------------------------------------------------------------------------------------#

package:
    stage: package
    script:
        - composer install $COMPOSER_FLAGS --no-dev
        - box compile
        # Create the artifact without the build folder
        - mv build/setup.phar ./setup.phar
    needs:
        - composer
        - unit-tests
    allow_failure: false
    artifacts:
        name: setup.phar
        paths:
            - setup.phar
        when: on_success
    cache:
        <<: *cache
        policy: pull
