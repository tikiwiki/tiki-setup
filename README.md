# Tiki Setup Application

Tiki Setup is a standalone application that allows to download Composer and install Tiki vendor dependencies. And also set permissions to Tiki files and directories. It replaces setup.sh 

## Available commands

### Composer

`composer:download` - Download composer.phar.

`composer:run` - Run Composer and install packages.

### Permissions

`permission:advance <PERMISSION_LEVEL>` - Use predefined permission. Only for advanced users.

`permission:fix` - Fix file & directory permissions (classic default). Run this command as root is recommended.

`permission:open` - Open file and directory permissions (classic option)

Permission Levels

| Name           | Default SubDirs | Default SubFiles | Writeable SubDirs | Writeable SubFiles |
| -------------- | :-------------: | :--------------: | :---------------: | :----------------: |
| paranoia       | 0700            | 0600             | 0700              | 0600               |
| paranoia-suphp | 0701            | 0600             | 0701              | 0600               |
| sbox           | 0501            | 0501             | 0701              | 0701               |
| worry          | 0701            | 0604             | 0703              | 0606               |
| moreworry      | 0705            | 0604             | 0707              | 0606               |
| pain           | 0701            | 0606             | 0703              | 0606               |
| morepain       | 0705            | 0606             | 0707              | 0606               |
| mixed          | 0770            | 0660             | 0770              | 0660               |
| risky          | 0775            | 0664             | 0777              | 0666               |
| insane         | 0777            | 0777             | 0777              | 0777               |

---

## Development

- PHP 7.2
- Composer
- Box

## How use?

To build the PHAR package it is required to install `humbug/box` locally.

```bash
composer global require humbug/box
make
```

After run make command a file setup.phar will be generated in build folder.
